import Vue from 'vue'
import Router from 'vue-router'
import movies from '@/components/movies'
import theatres from '@/components/theatres'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            redirect:'/movies'
        },
        {
            path: '/movies',
            name: 'movies',
            component: movies,
        },
        {
            path:'/theatres',
            name:'theatres',
            component: theatres
        }
    ]
})
